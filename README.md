# SPRINKLE TIMER

The purpose of this page is to explain step by step the realization of a sprinkle timer based on ARDUINO NANO.

The board uses the following components :

 * an ARDUINO NANO
 * a 20x4 I2C display
 * a 12 keys matrix keypad
 * a MEANWELL IRM-05-5 power supply
 * a 2 relays module
 * a DS3231 RTC module
 * some passive components

### ELECTRONICS

The schema is made using KICAD.

### ARDUINO

The code is build using ARDUINO IDE 1.8.5.

### BLOG
A description in french here : https://riton-duino.blogspot.com/2019/06/arduino-micro-irrigation-automatisee.html

