
#include <Wire.h>
#include <Time.h>

#include "gpio.h"
#include "config.h"
#include "gui.h"
#include "ds3231.h"
#include "valve.h"
#include "flow.h"
#include "humidity.h"
#include "sprinkle_timer.h"

#define CLOSE_BTN           1
#define OPEN_BTN            2

struct manualOperation manualOp = { -1, -1, -1, -1, -1};
int operationState = NONE;


void blink(void)
{
  static int alive = true;

  lcd.setCursor(17, 0);
  lcd.print(alive ? ':' : ' ');
  alive = !alive;
}

void displayConfig(int index)
{
  char buf[25];
  int h, m, s;
  int humidity;

  long duration = eepromData.configuration.automaticOp[index].duration;
  sprintf(buf, "ARROSAGE %d: %02dH%02d   ", index + 1, eepromData.configuration.automaticOp[index].hour, eepromData.configuration.automaticOp[index].minute);
  lcd.setCursor(0, 1);
  lcd.print(buf);

  h = duration / 3600;
  m = (duration - h / 3600) / 60;
  s = (duration - h / 3600) - (m * 60);
  sprintf(buf, "DUREE: %02d:%02d:%02d", h, m, s) ;
  lcd.setCursor(0, 2);
  lcd.print(buf);
  lcd.setCursor(0, 3);
  lcd.print(eepromData.configuration.enableSprinkler == true ? F("MARCHE        ") : F("ARRET         "));
#if HUMIDITY_SENSOR > 0
  lcd.setCursor(13, 3);
  lcd.print(getSoilMoisture(&humidity) == HUMIDITY_DRY ? F("SEC ") : F("HUM "));
  sprintf(buf, "%02d%%", humidity);
  lcd.print(buf);
#endif
}

void displayManual(void)
{
  char buf[20];
  long duration = manualOp.duration;
  int h, m, s;

  sprintf(buf, "MANUEL: %02d:%02d:%02d", manualOp.hour, manualOp.minute, manualOp.second);
  lcd.setCursor(0, 1);
  lcd.print(buf);
  h = duration / 3600;
  m = (duration - h / 3600) / 60;
  s = (duration - h / 3600) - (m * 60);
  sprintf(buf, "DUREE: %02d:%02d:%02d", h, m, s) ;
  lcd.setCursor(0, 2);
  lcd.print(buf);
}

void displayRemaining(long duration, long remain)
{
  char buf[20];
  int h, m, s;

  h = remain / 3600;
  m = (remain - h / 3600) / 60;
  s = remain - h / 3600 - m * 60;
  if (remain) {
    sprintf(buf, "%02d:%02d:%02d", h, m, s);
    lcd.setCursor(0, 3);
    lcd.print(buf);
  }
}

#if FLOW_SENSOR
void displayFlow(float flow)
{
  lcd.setCursor(11, 3);
  lcd.print(flow, 1);
  lcd.print(" L/MN ");
}
#endif

void startAutomaticOperation(int op)
{
  int humidity;
  if (getSoilMoisture(&humidity) == HUMIDITY_DRY) {
    operationState = RUNNING;
    displayConfig(op);
    openValve();
  }
}

void stopAutomaticOperation(void)
{
  digitalWrite(GREEN_LED_PIN, LOW);
  closeValve();
}

void startManualOperation(long duration)
{
  int second, minute, hour, dayOfWeek, dayOfMonth, month, year;

  operationState = RUNNING;
  readDS3231time(&second, &minute, &hour, &dayOfWeek, &dayOfMonth, &month, &year);
  manualOp.hour = hour;
  manualOp.minute = minute;
  manualOp.second = second;
  manualOp.duration = manualOp.remain = duration;
  openValve();
}

void resetManualOperation(void)
{
  closeValve();
}

void stopManualOperation(void)
{
  digitalWrite(GREEN_LED_PIN, LOW);
  resetManualOperation();
  closeValve();
}

int manualOperationInProgress(void)
{
  if (manualOp.hour >= 0 && manualOp.minute >= 0 && manualOp.second >= 0 && manualOp.remain > 0 && operationState != FLOW_ERROR) {
    return true;
  }
  return false;
}

void runManualOperation(void)
{
  int second = 0, minute = 0, hour = 0, dayOfWeek = 0, dayOfMonth = 0, month = 0, year = 0;
  long at, duration, remain;

  readDS3231time(&second, &minute, &hour, &dayOfWeek, &dayOfMonth, &month, &year);
  at = (long)manualOp.hour * 3600L + (long)manualOp.minute * 60L + (long)manualOp.second;
  duration = manualOp.duration;
  remain = manualOp.remain = manualOp.duration - ((long)hour * 3600L + (long)minute * 60L + (long)second - at);
  if (manualOp.remain == 0) {
    operationState = NONE;
    stopManualOperation();
  }
}

void abortOperation(const char *message)
{
  operationState = ABORTED;
  if (manualOperationInProgress()) {
    resetManualOperation();
  }
  closeValve();
  lcd.setCursor(0, 3);
  if (message) {
    lcd.print(message);
  }
  else {
    lcd.print(F("ARROSAGE INTERROMPU"));
  }
}

void setup()
{
  int second = 0, minute = 0, hour = 0, dayOfWeek = 0, dayOfMonth = 0, month = 0, year = 0;

  backlight(true);
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(GREEN_LED_PIN, OUTPUT);
  pinMode(RED_LED_PIN, OUTPUT);
  valveInit();
  flowInit();
  Serial.begin(115200);
  Serial.println(F("Sprinkle Timer"));
  Wire.begin();
  readConfig();
  readDS3231time(&second, &minute, &hour, &dayOfWeek, &dayOfMonth, &month, &year);
  if (second > 59 || minute > 59 || hour > 23 | dayOfMonth > 31 || year > 2050) {
    Serial.println(F("Invalid Time"));
    setDS3231time(0, 0, 0, 1, 1, 1, 2019);
  }
  printConfig();
  Serial.print(F("eepromData.crc: 0x")); Serial.println(eepromData.crc, HEX);
  guiInit();
  closeValve();
}

int getNextOperation(long now, long *at, long *duration)
{
  char buf[30];
  int second = 0, minute = 0, hour = 0, dayOfWeek = 0, dayOfMonth = 0, month = 0, year = 0;
  int op, count = 0, index = 0;
  long opTime[OPERATIONS];

  for (op = 0 ; op < OPERATIONS ; op++) {
    if (eepromData.configuration.automaticOp[op].duration) {
      count++;
    }
  }
  if (count == 0) {
    if (!manualOperationInProgress()) {
      lcd.setCursor(0, 1);
      lcd.print(F("AUCUNE PROGRAMMATION"));
      clearLine(2);
    }
    return -1;
  }
  for (op = 0 ; op < OPERATIONS ; op++) {
    opTime[op] = -1;
    if (eepromData.configuration.automaticOp[op].duration) {
      *at = (long)eepromData.configuration.automaticOp[op].hour * 3600L + (long)eepromData.configuration.automaticOp[op].minute * 60L;
      *duration = eepromData.configuration.automaticOp[op].duration;
      if (now < *at) {
        opTime[op] = *at - now;
        sprintf(buf, "Before time. Operation %d in %ld s", op, opTime[op]);
        Serial.println(buf);
      }
      else if (now >= *at && now <= *at + *duration) {
        opTime[op] = 0;
        sprintf(buf, "In time. Operation %d in %ld s", op, opTime[op]);
        Serial.println(buf);
      }
      else if (now > *at + *duration) {
        opTime[op] = *at + DAY_DURATION - now;
        sprintf(buf, "After time. Operation %d in %ld s", op, opTime[op]);
        Serial.println(buf);
      }
      sprintf(buf, "Operation %d in %ld s", op, opTime[op]);
      Serial.println(buf);
    }
  }
  for (op = 0 ; op < OPERATIONS ; op++) {
    if (opTime[op] != -1 && opTime[op] <= opTime[index]) {
      index = op;
      *at = (long)eepromData.configuration.automaticOp[op].hour * 3600L + (long)eepromData.configuration.automaticOp[op].minute * 60L;
      *duration = eepromData.configuration.automaticOp[op].duration;
    }
  }
  return index;
}

void turnOffLeds()
{
  digitalWrite(GREEN_LED_PIN, LOW);
  digitalWrite(RED_LED_PIN, LOW);
}

void manageLeds(void)
{
  static int alive;

  if (operationState == FLOW_ERROR) {
    digitalWrite(RED_LED_PIN, alive == 1 ? HIGH : LOW);
    alive = !alive;
  }
  if (operationState == RUNNING) {
    digitalWrite(GREEN_LED_PIN, alive == 1 ? HIGH : LOW);
    alive = !alive;
  }
}

float checkFlow(void)
{
  float flow = 0;
  if (operationState == RUNNING) {
    flow = getFlow();
    Serial.print(F("flow: ")); Serial.print(flow, 2); Serial.println("L/min");
    if (flow > eepromData.configuration.maxFlow) {
      if (operationState == RUNNING) {
        abortOperation("PROBLEME DE DEBIT");
      }
      operationState = FLOW_ERROR;
    }
  }
  return flow;
}

void loop()
{
  int second = 0, minute = 0, hour = 0, dayOfWeek = 0, dayOfMonth = 0, month = 0, year = 0;
  static int lastSec, lastMin = -1;

  if (eepromData.configuration.sleepMode && operationState == NONE) {
    backlight(false);
  }
  char key = keypad.getKey();
  if (key != NO_KEY) {
    if (runGui(key) == 1) {
      lastMin = -1; // force date & time refresh
    }
  }

  long now, at = -1;
  int op;
  long duration, remain;
  readDS3231time(&second, &minute, &hour, &dayOfWeek, &dayOfMonth, &month, &year);
  if (lastMin != minute) {
    displayTime(second, minute, hour, dayOfWeek, dayOfMonth,  month, year);
    lastMin = minute;
  }
  if (lastSec != second) {
    now = (long)hour * 3600L + (long)minute * 60L + (long)second;
    if (manualOperationInProgress()) {
      runManualOperation();
    }
    else {
      op = getNextOperation(now, &at, &duration);
      if (op != -1) {
        remain = duration - (now - at);
      }
      if (eepromData.configuration.enableSprinkler == true) {
        if (now > at && now < at + duration) {
          if (operationState == NONE) {
            startAutomaticOperation(op);
          }
        }
        else if (now == at + duration && operationState != FLOW_ERROR) {
          operationState = NONE;
          stopAutomaticOperation();
        }
      }
      else {
        Serial.println(F("ARROSAGE ARRETE"));
      }
    }
    float flow = checkFlow();
    blink();
    if (at != -1) {
      if (now <  at || now > at + duration) {
        turnOffLeds();
      }
    }
    if (manualOperationInProgress()) {
      displayManual();
    }
    else {
      displayConfig(op);
    }
    if (operationState == RUNNING) {
      if (manualOperationInProgress()) {
        Serial.print(F("In time, remain ")); Serial.println(manualOp.remain);
        displayRemaining(manualOp.duration, manualOp.remain);
      }
      else {
        Serial.print(F("In time, remain ")); Serial.println(remain);
        displayRemaining(duration, remain);
      }
#if FLOW_SENSOR
      displayFlow(flow);
#endif
    }
    lastSec = second;
    manageLeds();
  }
}
