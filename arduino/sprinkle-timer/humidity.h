
#ifndef _HUMIDITY_H_
#define _HUMIDITY_H_

/*
 * #define HUMIDITY_AIR  700 : air humidity
 * #define HUMIDITY_WATER 290 : water humidity
 */

#define HUMIDITY_AIR        590
#define HUMIDITY_WATER      250

#define HUMIDITY_DRY        0
#define HUMIDITY_WET        1

int getSoilMoisture(int *value);

#endif
