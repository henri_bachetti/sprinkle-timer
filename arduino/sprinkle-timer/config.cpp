
#include <Arduino.h>
#include <EEPROM.h>

#include "config.h"

struct eeprom_data eepromData;

unsigned long eeprom_crc(int sz) {

  const unsigned long crc_table[16] = {
    0x00000000, 0x1db71064, 0x3b6e20c8, 0x26d930ac,
    0x76dc4190, 0x6b6b51f4, 0x4db26158, 0x5005713c,
    0xedb88320, 0xf00f9344, 0xd6d6a3e8, 0xcb61b38c,
    0x9b64c2b0, 0x86d3d2d4, 0xa00ae278, 0xbdbdf21c
  };

  unsigned long crc = 0L;

  for (int index = 0 ; index < sz  ; ++index) {
    crc = crc_table[(crc ^ EEPROM[index]) & 0x0f] ^ (crc >> 4);
    crc = crc_table[(crc ^ (EEPROM[index] >> 4)) & 0x0f] ^ (crc >> 4);
    crc = ~crc;
  }
  return crc;
}

void printConfig(void)
{
  int op;

  for (op = 0 ; op < OPERATIONS ; op++) {
    Serial.print("eepromData.configuration.automaticOp["); Serial.print(op); Serial.print("].hour: "); Serial.println(eepromData.configuration.automaticOp[op].hour);
    Serial.print("eepromData.configuration.automaticOp["); Serial.print(op); Serial.print("].minute: "); Serial.println(eepromData.configuration.automaticOp[op].minute);
    Serial.print("eepromData.configuration.automaticOp["); Serial.print(op); Serial.print("].duration: "); Serial.println(eepromData.configuration.automaticOp[op].duration);
  }
  Serial.print(F("eepromData.configuration.enableSprinkler: ")); Serial.println(eepromData.configuration.enableSprinkler);
  Serial.print(F("eepromData.configuration.openTime: ")); Serial.println(eepromData.configuration.openTime);
  Serial.print(F("eepromData.configuration.closeTime: ")); Serial.println(eepromData.configuration.closeTime);
  Serial.print(F("eepromData.configuration.maxFlow: ")); Serial.println(eepromData.configuration.maxFlow);
  Serial.print(F("eepromData.configuration.maxHumidity: ")); Serial.println(eepromData.configuration.maxHumidity);
  Serial.print(F("eepromData.configuration.sleepMode: ")); Serial.println(eepromData.configuration.sleepMode);
}

void readConfig(void)
{
  Serial.print("eepromData.size: "); Serial.println(EEPROM.length());
  EEPROM.get(0, eepromData);
  if (eeprom_crc(sizeof(struct config)) != eepromData.crc) {
    Serial.println("invalid eeprom CRC");
    memset(eepromData.configuration.automaticOp, 0, sizeof(eepromData.configuration.automaticOp));
    eepromData.configuration.automaticOp[0].hour = 7;
    eepromData.configuration.automaticOp[0].minute = 0;
    eepromData.configuration.automaticOp[0].duration = 900L;
    eepromData.configuration.automaticOp[1].hour = 19;
    eepromData.configuration.automaticOp[1].minute = 0;
    eepromData.configuration.automaticOp[1].duration = 900L;
    eepromData.configuration.enableSprinkler = true;
    eepromData.configuration.openTime = 20;
    eepromData.configuration.closeTime = 20;
    eepromData.configuration.maxFlow = 10;
    eepromData.configuration.maxHumidity = 50;
    eepromData.configuration.sleepMode = 0;
    saveConfig();
  }
}

void saveConfig(void)
{
  Serial.println("saving configuration");
  printConfig();
  EEPROM.put(0, eepromData.configuration);
  eepromData.crc = eeprom_crc(sizeof(struct config));
  Serial.print("eepromData.crc: 0x"); Serial.println(eepromData.crc, HEX);
  EEPROM.put(sizeof(struct config), eepromData.crc);
}
