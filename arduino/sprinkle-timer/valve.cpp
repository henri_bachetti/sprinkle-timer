
#include <Arduino.h>

#include "gpio.h"
#include "config.h"
#include "gui.h"
#include "valve.h"

/*
 * RELAY_COMMAND : HIGH or LOW
 */

#define RELAY_COMMAND       LOW

static int valveState = UNKNOWN;

void valveInit(void)
{
  pinMode(VALVE_OPEN_PIN, OUTPUT);
  pinMode(VALVE_CLOSE_PIN, OUTPUT);
  digitalWrite(VALVE_OPEN_PIN, !RELAY_COMMAND);
  digitalWrite(VALVE_CLOSE_PIN, !RELAY_COMMAND);
}

void openValve(void)
{
  if (valveState != VALVE_OPEN) {
    backlight(true);
    digitalWrite(GREEN_LED_PIN, HIGH);
    lcd.setCursor(0, 3);
    lcd.print(F("OUVERTURE VANNE     "));
    Serial.println(F("Open Valve"));
    if (eepromData.configuration.openTime && eepromData.configuration.openTime) {
      digitalWrite(VALVE_OPEN_PIN, RELAY_COMMAND);
      waitForKey(eepromData.configuration.openTime);
      digitalWrite(VALVE_OPEN_PIN, !RELAY_COMMAND);
    }
    else {
      digitalWrite(VALVE_OPEN_PIN, RELAY_COMMAND);
    }
    valveState = VALVE_OPEN;
    Serial.println(F("OK"));
    clearLine(3);
    digitalWrite(GREEN_LED_PIN, LOW);
  }
}

void closeValve(void)
{
  if (valveState != VALVE_CLOSED) {
    digitalWrite(RED_LED_PIN, HIGH);
    lcd.setCursor(0, 3);
    backlight(true);
    lcd.print(F("FERMETURE VANNE     "));
    Serial.println(F("Close Valve"));
    if (eepromData.configuration.openTime && eepromData.configuration.closeTime) {
      digitalWrite(VALVE_CLOSE_PIN, RELAY_COMMAND);
      waitForKey(eepromData.configuration.closeTime);
      digitalWrite(VALVE_CLOSE_PIN, !RELAY_COMMAND);
    }
    else {
      digitalWrite(VALVE_OPEN_PIN, !RELAY_COMMAND);
    }
    valveState = VALVE_CLOSED;
    Serial.println(F("OK"));
    clearLine(3);
    digitalWrite(RED_LED_PIN, LOW);
  }
}
