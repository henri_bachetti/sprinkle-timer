
#ifndef _CONFIG_H_
#define _CONFIG_H_

#define OPERATIONS          9

struct manualOperation
{
  int hour;
  int minute;
  int second;
  long duration;
  long remain;
};

struct automaticOperation
{
  int hour;
  int minute;
  long duration;
};

struct config
{
  struct automaticOperation automaticOp[OPERATIONS];
  int enableSprinkler;
  int openTime;
  int closeTime;
  int sleepMode;
  int maxFlow;
  int maxHumidity;
};

struct eeprom_data
{
  struct config configuration;
  unsigned long crc;
};

extern struct eeprom_data eepromData;
extern struct manualOperation manualOp;

void printConfig(void);
void readConfig(void);
void saveConfig(void);

#endif
