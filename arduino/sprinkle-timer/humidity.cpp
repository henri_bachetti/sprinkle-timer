
#include <Arduino.h>

#include "humidity.h"
#include "config.h"
#include "gpio.h"

int getSoilMoisture(int *value)
{
#if HUMIDITY_SENSOR > 0
  unsigned percent = 100-eepromData.configuration.maxHumidity;  // reverse percentage
  unsigned intervals = (HUMIDITY_AIR - HUMIDITY_WATER) / (100 / percent);
  unsigned limit = HUMIDITY_WATER + intervals;
  unsigned soilMoistureValue = analogRead(HUMIDITY_SENSOR);
  unsigned maxDiff = HUMIDITY_AIR - HUMIDITY_WATER;
  unsigned tmp = HUMIDITY_AIR - soilMoistureValue;
  unsigned diff = maxDiff -tmp;
  *value = 100 - (diff * 100 / maxDiff);
  Serial.print(F("Humidity: "));
  Serial.print(soilMoistureValue);
  Serial.print(F(" (limit "));
  Serial.print(limit);
  Serial.print(')');
  if (soilMoistureValue < (limit)) {
    Serial.println(" Wet");
    return HUMIDITY_WET;
  }
#endif
  Serial.println(" Dry");
  return HUMIDITY_DRY;
}
