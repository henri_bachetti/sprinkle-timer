
#ifndef _VALVE_H_
#define _VALVE_H_

#define UNKNOWN             -1
#define VALVE_CLOSED        0
#define VALVE_OPEN          1

void valveInit(void);
void openValve(void);
void closeValve(void);

#endif
