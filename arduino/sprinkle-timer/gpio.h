
#ifndef _GPIO_H_
#define _GPIO_H_

/*
 * #define I2C_LCD : use an I2C LCD display + UNO board
 * //#define I2C_LCD : use a parellel LCD display + MEGA board
 */

#define I2C_LCD

/*
 * #define KBD_ACCORD : use an ACCORD KB304 or equivalent. pinout = c2, r1, c1, r4, c3, r3, r2
 * //#define KBD_ACCORD : use a keypad with straight pinout r1, r2, r3, r4, c1, c2, c3
 */

#define KBD_ACCORD

/*
 * #define FLOW_SENSOR   2 : use the flow sensor
 * #define FLOW_SENSOR   0 : don't use the flow sensor
 */

#define FLOW_SENSOR         2

/*
 * #define HUMIDITY_SENSOR  2 : use the humidity sensor on analog A2
 * #define HUMIDITY_SENSOR  0 : don't use the humidity sensor
 */

#define HUMIDITY_SENSOR     2

#ifndef I2C_LCD
#define LCD_RS              12
#define LCD_EN              11
#define LCD_D4              5
#define LCD_D5              4
#define LCD_D6              3
#define LCD_D7              2
#define BACKLIGHT_PIN       22
#define KBD_COL2            10
#define KBD_ROW1            9
#define KBD_COL1            8
#define KBD_ROW4            17
#define KBD_COL3            16
#define KBD_ROW3            15
#define KBD_ROW2            14
#define VALVE_OPEN_PIN      6
#define VALVE_CLOSE_PIN     7
#define GREEN_LED_PIN       18
#define RED_LED_PIN         19
#else
#define LCD_I2C_ADDRESS     0x27
#ifdef KBD_ACCORD
#define KBD_COL2            4
#define KBD_ROW1            5
#define KBD_COL1            6
#define KBD_ROW4            7
#define KBD_COL3            8
#define KBD_ROW3            9
#define KBD_ROW2            10
#else
#define KBD_ROW1            4
#define KBD_ROW2            5
#define KBD_ROW3            6
#define KBD_ROW4            7
#define KBD_COL1            8
#define KBD_COL2            9
#define KBD_COL3            10
#endif
#define VALVE_OPEN_PIN      A0
#define VALVE_CLOSE_PIN     A1
#define GREEN_LED_PIN       11
#define RED_LED_PIN         12
#endif

#define DS3231_I2C_ADDRESS  0x68

#endif
