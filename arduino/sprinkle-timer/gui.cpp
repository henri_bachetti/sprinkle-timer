
#include <Arduino.h>

#include "gpio.h"
#include "gui.h"

#ifndef I2C_LCD
LiquidCrystal lcd(LCD_RS, LCD_EN, LCD_D4, LCD_D5, LCD_D6, LCD_D7);
#else
LiquidCrystal_I2C lcd(LCD_I2C_ADDRESS, DISPLAY_COLS, DISPLAY_ROWS);
#endif

// keyboard is ACCORD KB304-PNB
byte rowPins[ROWS] = {KBD_ROW1, KBD_ROW2, KBD_ROW3, KBD_ROW4};
byte colPins[COLS] = {KBD_COL1, KBD_COL2, KBD_COL3};

char keys[ROWS][COLS] = {
  {'1', '2', '3'},
  {'4', '5', '6'},
  {'7', '8', '9'},
  {'*', '0', '#'}
};

Keypad keypad = Keypad(makeKeymap(keys), rowPins, colPins, ROWS, COLS);

void guiInit(void)
{
#ifndef I2C_LCD
  pinMode(BACKLIGHT_PIN, OUTPUT);
#endif
  keypad.setHoldTime(4);
  keypad.setDebounceTime(4);
#ifndef I2C_LCD
  lcd.begin(DISPLAY_COLS, DISPLAY_ROWS);
#else
  lcd.begin();
#endif
  lcd.clear();
}

void backlight(bool state)
{
#ifndef I2C_LCD
  digitalWrite(BACKLIGHT_PIN, state);
#else
  if (state == true) {
    lcd.backlight();
  }
  else {
    lcd.noBacklight();
  }
#endif
  if (state == true) {
    lcd.display();
  }
  else {
    lcd.noDisplay();
  }
}

void clearLine(int row)
{
  lcd.setCursor(0, row);
  lcd.print("                    ");
}

void displayTime(int second, int minute, int hour, int dayOfWeek, int dayOfMonth, int month, int year)
{
  char buf[20];

  clearLine(0);
  sprintf(buf, "%02d.%02d.%d", dayOfMonth, month, year);
  lcd.setCursor(0, 0);
  lcd.print(buf);
  sprintf(buf, "%02d:%02d", hour, minute);
  lcd.setCursor(15, 0);
  lcd.print(buf);
}

static const char *substr(char *s, int from, int to)
{
  static char tmp[10];
  int len = strlen(s);
  memcpy(tmp, s + from, to - from);
  tmp[to - from] = 0;
  return tmp;
}

static void displayIntegerField(char *s, int len, int row, int col)
{
  Serial.println(s);
  lcd.setCursor(col, row);
  lcd.print(s);
}

static void displayDateField(char *s, int len, int row, int col)
{
  char buf[12];

  Serial.println(s);
  sprintf(buf, "%c%c-%c%c-%c%c%c%c", s[0], s[1], s[2], s[3], s[4], s[5], s[6], s[7]);
  lcd.setCursor(col, row);
  lcd.print(buf);
}

static void displayTimeField(char *s, int len, int row, int col)
{
  char buf[10];

  Serial.println(s);
  if (len == 4) {
    sprintf(buf, "%c%c:%c%c", s[0], s[1], s[2], s[3]);
  }
  else {
    sprintf(buf, "%c%c:%c%c:%c%c", s[0], s[1], s[2], s[3], s[4], s[5]);
  }
  lcd.setCursor(col, row);
  lcd.print(buf);
}

void waitForKey(int seconds)
{
  seconds *= 10;
  while (seconds--) {
    delay(100);
    if (keypad.getKey() != NO_KEY) {
      Serial.println("Interrupted");
      break;
    }
  }
}

int Form::get(const char *question, char *entry, int len, void displayFunc(char *, int, int, int))
{
  int count = strlen(entry);
  char key;
  char copy[20];

  strcpy(copy, entry);
  lcd.clear();
  lcd.print(question);
  lcd.setCursor(0, 1);
  displayFunc(entry, count, 1, 0);
  while (1) {
    key = keypad.waitForKey();
    Serial.print("Form::get: ");
    Serial.println(key);
    switch (key) {
      case '#':
        if (count == len) {
          lcd.clear();
          return strcmp(copy, entry) ? 1 : 0;
        }
        break;
      case '*':
        if (count > 0) {
          entry[--count] = ' ';
        }
        displayFunc(entry, len, 1, 0);
        break;
      default:
        if (count < len) {
          entry[count++] = key;
          entry[count] = 0;
        }
        else {
          memcpy(entry, entry + 1, len - 1);
          entry[len - 1] = key;
        }
        displayFunc(entry, len, 1, 0);
        break;
    }
  }
}

int Form::getBool(const char *question, char *entry)
{
  char key;
  char copy[20];

  strcpy(copy, entry);
  lcd.clear();
  lcd.print(question);
  lcd.setCursor(0, 1);
  displayIntegerField(entry, 1, 1, 0);
  while (1) {
    key = keypad.waitForKey();
    Serial.print("Form::getBool: ");
    Serial.println(key);
    switch (key) {
      case '#':
        lcd.clear();
        return strcmp(copy, entry) ? 1 : 0;
      case '0':
      case '1':
        entry[0] = key;
        entry[1] = 0;
        displayIntegerField(entry, 1, 1, 0);
        break;
      default:
        break;
    }
  }
}

int Form::getDate(const char *question, int *year, int *month, int *day)
{
  char buf[20];
  int status;

  sprintf(buf, "%02d%02d%04d", *day, *month, *year);
  status = this->get(question, buf, 8, displayDateField);
  if (status == true) {
    *day = atoi(substr(buf, 0, 2));
    *month = atoi(substr(buf, 2, 4));
    *year = atoi(substr(buf, 4, 8));
  }
  return status;
}

int Form::getTime(const char *question, int *hour, int *minute, int *second)
{
  char buf[20];
  int status;

  sprintf(buf, "%02d%02d%02d", *hour, *minute, *second);
  status = this->get(question, buf, 6, displayTimeField);
  if (status == true) {
    *hour = atoi(substr(buf, 0, 2));
    *minute = atoi(substr(buf, 2, 4));
    *second = atoi(substr(buf, 4, 6));
  }
  return status;
}

int Form::getTime(const char *question, int *hour, int *minute)
{
  char buf[20];
  int status;

  sprintf(buf, "%02d%02d", *hour, *minute);
  status = this->get(question, buf, 4, displayTimeField);
  if (status == true) {
    *hour = atoi(substr(buf, 0, 2));
    *minute = atoi(substr(buf, 2, 4));
  }
  return status;
}

int Form::getInteger(const char *question, int *i, const char *fmt)
{
  char buf[20];
  int status;

  sprintf(buf, fmt, *i);
  Serial.print("Form::getInteger: ");
  Serial.println(buf);
  status = this->get(question, buf, strlen(buf), displayIntegerField);
  if (status == true) {
    *i = atoi(buf);
  }
  return status;
}

int Form::getBoolean(const char *question, int *i)
{
  char buf[2];
  int status;

  sprintf(buf, "%d", *i);
  Serial.print("Form::getBoolean: ");
  Serial.println(buf);
  status = this->getBool(question, buf);
  if (status == true) {
    *i = atoi(buf);
  }
  return status;
}
