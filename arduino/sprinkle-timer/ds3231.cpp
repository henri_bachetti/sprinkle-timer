
#include <Arduino.h>
#include <Wire.h>

#include "gpio.h"
#include "ds3231.h"

// Convert normal decimal numbers to binary coded decimal
static byte decToBcd(byte val)
{
  return ( (val / 10 * 16) + (val % 10) );
}

// Convert binary coded decimal to normal decimal numbers
static byte bcdToDec(byte val)
{
  return ( (val / 16 * 10) + (val % 16) );
}

void setDS3231time(int second, int minute, int hour, int dayOfWeek, int dayOfMonth, int month, int year)
{
  Serial.print(F("Writing DS3231: ")); Serial.print(year); Serial.print(F("-")); Serial.print(month); Serial.print(F("-")); Serial.println(dayOfMonth); 
  // sets time and date data to DS3231
  Wire.beginTransmission(DS3231_I2C_ADDRESS);
  Wire.write(0); // set next input to start at the seconds register
  Wire.write(decToBcd(second)); // set seconds
  Wire.write(decToBcd(minute)); // set minutes
  Wire.write(decToBcd(hour)); // set hours
  Wire.write(decToBcd(dayOfWeek)); // set day of week (1=Sunday, 7=Saturday)
  Wire.write(decToBcd(dayOfMonth)); // set date (1 to 31)
  Wire.write(decToBcd(month)); // set month
  Wire.write(decToBcd(year-2000)); // set year (0 to 99)
  Wire.endTransmission();
}

void readDS3231time(int *second, int *minute, int *hour, int *dayOfWeek, int *dayOfMonth, int *month, int *year)
{
  Wire.beginTransmission(DS3231_I2C_ADDRESS);
  Wire.write(0); // set DS3231 register pointer to 00h
  Wire.endTransmission();
  Wire.requestFrom(DS3231_I2C_ADDRESS, 7);
  // request seven bytes of data from DS3231 starting from register 00h
  *second = bcdToDec(Wire.read() & 0x7f);
  *minute = bcdToDec(Wire.read());
  *hour = bcdToDec(Wire.read() & 0x3f);
  *dayOfWeek = bcdToDec(Wire.read());
  *dayOfMonth = bcdToDec(Wire.read());
  *month = bcdToDec(Wire.read());
  *year = bcdToDec(Wire.read()) + 2000;
//  Serial.print(F("Reading DS3231: ")); Serial.print(*year); Serial.print(F("-")); Serial.print(*month); Serial.print(F("-")); Serial.println(*dayOfMonth); 
}

