
#ifndef _DISPLAY_H_
#define _DISPLAY_H_

#include "gpio.h"

#ifndef I2C_LCD
#include <LiquidCrystal.h>
#else
#include <LiquidCrystal_I2C.h>
#endif
#include <Keypad.h>

#define DISPLAY_COLS        20
#define DISPLAY_ROWS        4
#define ROWS                4
#define COLS                3

#ifndef I2C_LCD
extern LiquidCrystal lcd;
#else
extern LiquidCrystal_I2C lcd;
#endif

extern Keypad keypad;

class Form
{
  public:
    int get(const char *question, char *entry, int len, void displayFunc(char *, int, int, int));
    int getBool(const char *question, char *entry);
    int getInteger(const char *question, int *i, const char *fmt);
    int getBoolean(const char *question, int *i);
    int getDate(const char *question, int *year, int *month, int *day);
    int getTime(const char *question, int *hour, int *minute, int *second);
    int getTime(const char *question, int *hour, int *minute);
};

void guiInit(void);
void backlight(bool state);
void clearLine(int row);
void waitForKey(int seconds);
void displayTime(int second, int minute, int hour, int dayOfWeek, int dayOfMonth, int month, int year);

#endif
