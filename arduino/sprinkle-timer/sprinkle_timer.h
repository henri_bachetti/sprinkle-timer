
#define NO_EDIT             0x00
#define DATETIME_EDIT       0x01
#define CONFIG_EDIT         0x02

#define DAY_DURATION        (24L*60L*60L)
#define NONE                0
#define RUNNING             1
#define ABORTED             2
#define FLOW_ERROR          3

extern int operationState;

void startAutomaticOperation(int op);
void stopAutomaticOperation(void);
void startManualOperation(long duration);
void resetManualOperation(void);
void stopManualOperation(void);
int manualOperationInProgress(void);
void runManualOperation(void);
void abortOperation(const char *message);
int runGui(int key);
