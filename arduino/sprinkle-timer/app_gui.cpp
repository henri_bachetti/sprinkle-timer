
#include "config.h"
#include "ds3231.h"
#include "gui.h"
#include "sprinkle_timer.h"

int runGui(int key)
{
  int len;
  int edit = false;
  int second = 0, minute = 0, hour = 0, dayOfWeek = 0, dayOfMonth = 0, month = 0, year = 0;
  Form form;
  int modified = 0;

  backlight(true);
  Serial.print(F("Key: ")); Serial.println(key);
  switch (key) {
    case '#':
      readDS3231time(&second, &minute, &hour, &dayOfWeek, &dayOfMonth, &month, &year);
      if (form.getDate("DATE:", &year, &month, &dayOfMonth) == 1) {
        edit |= DATETIME_EDIT;
      }
      if (form.getTime("HEURE:", &hour, &minute) == true) {
        edit |= DATETIME_EDIT;
      }
      if (edit & DATETIME_EDIT) {
        setDS3231time(0, minute, hour, dayOfWeek, dayOfMonth, month, year);
      }
      if (form.getBoolean("MARCHE:", &eepromData.configuration.enableSprinkler) == true) {
        edit |= CONFIG_EDIT;
      }
      if (form.getTime("ARROSAGE:", &eepromData.configuration.automaticOp[0].hour, &eepromData.configuration.automaticOp[0].minute) == true) {
        edit |= CONFIG_EDIT;
      }
      int h, m, s;
      h = eepromData.configuration.automaticOp[0].duration / 3600;
      m = (eepromData.configuration.automaticOp[0].duration - h / 3600) / 60;
      s = eepromData.configuration.automaticOp[0].duration - h / 3600 - m * 60;
      if (form.getTime("DUREE:", &h, &m, &s) == true) {
        edit |= CONFIG_EDIT;
        eepromData.configuration.automaticOp[0].duration = h * 3600 + m * 60 + s;
      }
      if (form.getInteger("OUVERTURE VANNE:", &eepromData.configuration.openTime, "%02d") == true) {
        edit |= CONFIG_EDIT;
      }
      if (form.getInteger("FERMETURE VANNE:", &eepromData.configuration.closeTime, "%02d") == true) {
        edit |= CONFIG_EDIT;
      }
#if FLOW_SENSOR
      if (form.getInteger("DEBIT MAXIMUM:", &eepromData.configuration.maxFlow, "%02d") == true) {
        edit |= CONFIG_EDIT;
      }
#endif
#if HUMIDITY_SENSOR
      if (form.getInteger("HUMIDITE MAXIMALE:", &eepromData.configuration.maxHumidity, "%02d") == true) {
        edit |= CONFIG_EDIT;
      }
#endif
      if (form.getBoolean("VEILLE:", &eepromData.configuration.sleepMode) == true) {
        edit |= CONFIG_EDIT;
      }
      if (edit & CONFIG_EDIT) {
        saveConfig();
      }
      modified = 1;
      break;
    case '*':
      if (operationState == RUNNING) {
        abortOperation(NULL);
      }
      else {
        int s = 0, m = 0, h = 0;
        long duration;
        form.getTime("DUREE:", &h, &m, &s);
        duration = (long)h * 3600L + (long)m * 60L + (long)s;
        if (duration) {
          startManualOperation(duration);
        }
      }
      modified = 1;
      break;
    default: {
        int index = key - '1';
        if (key == '0') {
          guiInit();
        }
        else {
          char prompt[10];
          sprintf(prompt, "ARROSAGE %c", key);
          if (form.getTime(prompt, &eepromData.configuration.automaticOp[index].hour, &eepromData.configuration.automaticOp[index].minute) == true) {
            edit |= CONFIG_EDIT;
          }
          int h, m, s;
          h = eepromData.configuration.automaticOp[index].duration / 3600;
          m = (eepromData.configuration.automaticOp[index].duration - h / 3600) / 60;
          s = eepromData.configuration.automaticOp[index].duration - h / 3600 - m * 60;
          if (form.getTime("DUREE:", &h, &m, &s) == true) {
            edit |= CONFIG_EDIT;
            eepromData.configuration.automaticOp[index].duration = h * 3600 + m * 60 + s;
          }
          if (edit & CONFIG_EDIT) {
            saveConfig();
          }
        }
      }
      modified = 1;
      break;
  }
  return modified;
}
