
#ifndef _DS3231_H_
#define _DS3231_H_

void setDS3231time(int second, int minute, int hour, int dayOfWeek, int dayOfMonth, int month, int year);
void readDS3231time(int *second, int *minute, int *hour, int *dayOfWeek, int *dayOfMonth, int *month, int *year);

#endif

