
#include <Arduino.h>

#include "gpio.h"
#include "flow.h"

byte sensorInterrupt = 0;

float calibrationFactor = 7.5;

static volatile byte pulseCount;
static unsigned long total;

unsigned long oldTime;

void pulseCounter()
{
  pulseCount++;
}

void flowInit(void)
{
#if FLOW_SENSOR > 0
  pinMode(FLOW_SENSOR, INPUT);
  attachInterrupt(sensorInterrupt, pulseCounter, FALLING);
#endif
}

float getFlow(void)
{
  float flowRate = 0;
  unsigned int milliLiters;

#if FLOW_SENSOR > 0
  detachInterrupt(sensorInterrupt);
  flowRate = ((1000.0 / (millis() - oldTime)) * pulseCount) / calibrationFactor;
  oldTime = millis();
  milliLiters = (flowRate / 60) * 1000;
  total += milliLiters;
  pulseCount = 0;
  attachInterrupt(sensorInterrupt, pulseCounter, FALLING);
#endif
  return flowRate;
}

unsigned long getTotalMilliLiters(void)
{
  return total;
}

